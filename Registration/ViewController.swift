//
//  ViewController.swift
//  Registration
//
//  Created by Ostafi Ion on 10/1/16.
//  Copyright © 2016 Ostafi Ion. All rights reserved.
//

import UIKit
import DropDown

class RegistrationViewController: UIViewController {
    @IBOutlet var countryDropDown: UIButton!
    @IBOutlet var cityDropDown: UIButton!
    @IBOutlet var countryDropDownAnchorView: UIView!
    @IBOutlet var cityDropDownAnchorView: UIView!
    @IBOutlet var personalInfoView: UIView!
    @IBOutlet var contactInfoView: UIView!
    
    lazy var cityDropDownList: DropDown = { [unowned self] in
        let tempDropDown = DropDown()
        tempDropDown.dataSource = ["San Francisco", "Daily City", "Foster City"]
        tempDropDown.anchorView = self.cityDropDownAnchorView
        return tempDropDown
    }()
    
    lazy var countryDropDownList: DropDown = { [unowned self] in
        let tempDropDown = DropDown()
        tempDropDown.dataSource = ["United States", "Canada", "Mexico"]
        tempDropDown.anchorView = self.countryDropDownAnchorView
        return tempDropDown
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.setupInformationViews()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupDropDownButton(dropDownButton: countryDropDown)
        setupDropDownButton(dropDownButton: cityDropDown)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didPressCountry(sender: UIButton) {
        countryDropDownList.show()
        countryDropDownList.selectionAction = { [unowned sender] (_, item: String) in
            sender.setTitle(item, for: .normal)
        }
    }
    
    @IBAction func didPressCity(sender: UIButton) {
        cityDropDownList.show()
        cityDropDownList.selectionAction = { [unowned sender] (_, item: String) in
            sender.setTitle(item, for: .normal)
        }
    }

    @IBAction func didPressProfileImage() {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            let alert = UIAlertController(title: "Error", message: "Cannot open image library", preferredStyle: UIAlertControllerStyle.alert)
            return self.present(alert, animated: true, completion: nil)
        }
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    private func setupNavigationBar() {
        guard let navBar = navigationController?.navigationBar else { return }
        navBar.setBackgroundImage(UIImage(named: "nav_bar_background"), for: .default)
        navBar.shadowImage = UIImage()
        navBar.topItem?.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_bar_back_arrow"), style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        navBar.topItem?.rightBarButtonItem = UIBarButtonItem(title: "Skip", style: .plain, target: nil, action: nil)
    }
    
    private func setupDropDownButton(dropDownButton: UIButton) {
        let buttonWidth = dropDownButton.bounds.size.width
        let arrowWidth = dropDownButton.imageView!.frame.size.width
        dropDownButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: buttonWidth - arrowWidth,
                                                       bottom: 0, right: 0)
        dropDownButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: -(buttonWidth),
                                                      bottom: 0, right: 0)
    }
    
    private func setupInformationViews() {
        let borderColor = UIColor(red: 216 / 255, green: 224 / 255, blue: 232 / 255, alpha: 1)
        personalInfoView.layer.borderWidth = 1
        personalInfoView.layer.borderColor = borderColor.cgColor
        contactInfoView.layer.borderWidth = 1
        contactInfoView.layer.borderColor = borderColor.cgColor
    }
}

